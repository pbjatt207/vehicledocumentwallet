import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";
import { API_BASE } from "../configs/constants.config";

const ApiExecute = async (url, params = {}) => {
    console.log(API_BASE + url);
    let headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
    let instance = axios.create({
        baseURL: API_BASE,
        timeout: 1000,
        headers
    });

    let method = params.method ?? 'GET';
    let data = params.data ?? null;

    return instance.request({
        url,
        method,
        data
    })
        .then(res => {
            console.log('response: ', res);
            return {
                status: true,
                data: res.data
            }
        })
        .catch(err => {
            console.log('API Error: ', url, err);
            return {
                status: false,
                error: err
            }
        });
}
export default ApiExecute