import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";

const searchApiExecute = async (url, params = {}) => {
    let headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'API-KEY': 'ab4107ef95b62c89cdc06eeb96c75e3a',
        'Referer': 'https://iagency.in/'
    }
    
    let instance = axios.create({
        baseURL: 'https://api.apiclub.in/api/v1/',
        timeout: 1000,
        headers
    });

    let method = params.method ?? 'GET';
    let data = params.data ?? null;
    let form_data = new FormData();


    for (const field in data) {
        form_data.append(field, data[field]);
    }

    return instance.request({
        url,
        method,
        data: form_data
    })
    
        .then(res => {
            console.log('response: ', res);
            return {
                status: true,
                data: res.data
            }
        })
        .catch(err => {
            console.log('API Error: ', url, err);
            return {
                status: false,
                error: err
            }
        });
}
export default searchApiExecute