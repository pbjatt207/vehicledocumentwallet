import React, { Component } from "react";
import { Icon } from "react-native-elements";
import { COLORS } from "../configs/constants.config";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import VehicleScreen from "../src/Vehicle/VehicleScreen";
import HomeScreen from "../src/Home/HomeScreen";
import Login1Screen from "../src/Login/Login1Screen";
import PosterScreen from "../src/Poster/PosterScreen";
import ArticleScreen from "../src/Article/ArticleScreen";
import InsuranceScreen from "../src/Insurance/InsuranceScreen";
import AsyncStorage from "@react-native-async-storage/async-storage";
import RScreen from "../src/Login/RScreen";
export default class NavTab extends Component {
    state = {
        user: null,
        language: 'en',
    };
    async componentDidMount() {
        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            console.log('user nav tab', user);
            this.setState({ user: user });
        }
        let lang = await AsyncStorage.getItem('@language');
        if (lang) {
            this.setState({ language: lang });
        }
    }
    render() {
        const Tab = createBottomTabNavigator();
        return (
            <>
                <Tab.Navigator
                    initialRouteName="Home"
                    activeColor={COLORS.white}
                    barStyle={{ backgroundColor: COLORS.primary }}
                    screenOptions={{
                        headerShown: false,
                        tabBarInactiveTintColor: '#ddd',
                        tabBarActiveTintColor: COLORS.white,
                        tabBarStyle: {
                            backgroundColor: COLORS.primary,
                            paddingBottom: 3
                        },
                        tabBarIconStyle: {
                            color: 'black'
                        }
                    }}
                >
                    <Tab.Group
                    >
                        <Tab.Screen
                            name="Home"
                            component={HomeScreen}
                            options={{
                                tabBarLabel: `${this.state.language == 'en' ? 'Home' : 'घर'}`,
                                tabBarIcon: ({ focused, color, size }) => (
                                    <Icon
                                        name={focused ? 'home' : 'home-outline'}
                                        type='ionicon'
                                        color={color}
                                        size={size}
                                    />
                                )
                            }}
                        />
                        {
                            this.state.user ?
                                <Tab.Screen
                                    name="Vehicle"
                                    component={VehicleScreen}
                                    options={{
                                        tabBarLabel: `${this.state.language == 'en' ? 'My Vehicle' : 'मेरा वाहन'}`,
                                        tabBarIcon: ({ focused, color, size }) => (
                                            <Icon
                                                name={focused ? 'car' : 'car-outline'}
                                                type='ionicon'
                                                color={color}
                                                size={size}
                                            />
                                        ),
                                    }}
                                />
                                :
                                <Tab.Screen
                                    name="Vehicle"
                                    component={RScreen}
                                    options={{
                                        tabBarLabel: `${this.state.language == 'en' ? 'My Vehicle' : 'मेरा वाहन'}`,
                                        tabBarIcon: ({ focused, color, size }) => (
                                            <Icon
                                                name={focused ? 'car' : 'car-outline'}
                                                type='ionicon'
                                                color={color}
                                                size={size}
                                            />
                                        ),
                                    }}
                                />
                        }
                        {
                            this.state.user ?
                                <Tab.Screen
                                    name="Insurance"
                                    component={InsuranceScreen}
                                    options={{
                                        tabBarLabel: `${this.state.language == 'en' ? 'Renew Insurance' : 'बीमा नवीनीकृत'}`,
                                        tabBarIcon: ({ focused, color, size }) => (
                                            <Icon
                                                name={focused ? 'ios-shield-checkmark' : 'ios-shield-checkmark-outline'}
                                                type='ionicon'
                                                color={color}
                                                size={size}
                                            />
                                        ),
                                    }}
                                />
                                :
                                <Tab.Screen
                                    name="Insurance"
                                    component={RScreen}
                                    options={{
                                        tabBarLabel: `${this.state.language == 'en' ? 'Renew Insurance' : 'बीमा नवीनीकृत'}`,
                                        tabBarIcon: ({ focused, color, size }) => (
                                            <Icon
                                                name={focused ? 'ios-shield-checkmark' : 'ios-shield-checkmark-outline'}
                                                type='ionicon'
                                                color={color}
                                                size={size}
                                            />
                                        ),
                                    }}
                                />
                        }
                        <Tab.Screen
                            name="Posters"
                            component={PosterScreen}
                            options={{
                                tabBarLabel: `${this.state.language == 'en' ? 'Posters' : 'पोस्टर'}`,
                                tabBarIcon: ({ focused, color, size }) => (
                                    // <MaterialCommunityIcons name="account" color={color} size={26} />
                                    <Icon
                                        name={focused ? 'images' : 'images-outline'}
                                        type='ionicon'
                                        color={color}
                                        size={size}
                                    />
                                ),
                            }}
                        />
                        <Tab.Screen
                            name="Articles"
                            component={ArticleScreen}
                            options={{
                                tabBarLabel: `${this.state.language == 'en' ? 'Articles' : 'सामग्री'}`,
                                tabBarIcon: ({ focused, color, size }) => (
                                    // <MaterialCommunityIcons name="account" color={color} size={26} />
                                    <Icon
                                        name={focused ? 'newspaper' : 'newspaper-outline'}
                                        type='ionicon'
                                        color={color}
                                        size={size}
                                    />
                                ),
                            }}
                        />
                    </Tab.Group>
                </Tab.Navigator>
            </>
        )

    }
}