import AsyncStorage from "@react-native-async-storage/async-storage";
import { DrawerContentScrollView } from "@react-navigation/drawer";
import React, { Component } from "react";
import { Image, Linking, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { COLORS } from "../configs/constants.config";
import { commanApi } from "../Api/api";

export default class DrawerContent extends Component {
    state = {
        user: null,
        language: 'en',
    }
    componentDidMount = async () => {
        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            this.setState({ user: user });
        }
        let lang = await AsyncStorage.getItem('@language');
        if (lang) {
            this.setState({ language: lang });
        }
    }
    navigateTo = (path, params = null) => {
        let { navigation } = this.props;

        navigation.navigate(path, params);
    }
    logOut = async () => {
        await AsyncStorage.setItem('@user', '');
        this.goTo('Login');
    }

    logOut = async () => {
        let { user } = this.state;
        let response = await commanApi(`logout?mobile=${user.mobile}`);
        if (response.status) {
            if (response.data.status) {
                await AsyncStorage.setItem('@user', '');
                this.goTo('Login');
            } else {
                Alert.alert(
                    "Error",
                    response.data.message,
                    [
                        { text: "OK", onPress: () => console.log("OK Pressed") }
                    ]
                );
            }
        }
    }
    goTo = path => {
        let { navigation } = this.props;
        navigation.navigate(path);
    }
    render() {
        let user = this.state.user;
        return (
            <>
                <View style={{ backgroundColor: '#f2f2f2', padding: 30 }}>
                    <Image
                        source={require('../assets/logo.png')}
                        // source={{ uri: "https://cdn-icons-png.flaticon.com/128/747/747376.png" }}
                        resizeMode="contain"
                        style={{ width: '100%', height: 100, }}
                    />
                    {/* <Text style={{ fontSize: 18, textAlign: 'center', color: '#fff', padding: 10 }}>Jhon Due</Text> */}
                </View>
                <DrawerContentScrollView {...this.props}>
                    {
                        this.state.user ?
                            <>
                                <TouchableOpacity style={style.list} onPress={() => this.navigateTo('Profile')}>
                                    <Text style={style.navList}>{this.state.language == 'en' ? 'Profile' : 'प्रोफ़ाइल'}</Text>
                                </TouchableOpacity>
                            </>
                            :
                            <>
                                <TouchableOpacity style={style.list} onPress={() => this.navigateTo('Login1')}>
                                    <Text style={style.navList}>{this.state.language == 'en' ? 'Login' : 'लॉग इन'}</Text>
                                </TouchableOpacity>
                            </>
                    }
                    <TouchableOpacity style={style.list} onPress={() => Linking.openURL('https://play.google.com/store/apps/details?id=com.vehiclewallet.iagency')}>
                        <Text style={style.navList}>{this.state.language == 'en' ? 'Rate this app' : 'Rate this app'}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={style.list} onPress={() => Linking.openURL('https://play.google.com/store/apps/details?id=com.vehiclewallet.iagency')}>
                        <Text style={style.navList}>{this.state.language == 'en' ? 'Share with friends' : 'Share with friends'}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={style.list} onPress={() => this.navigateTo('PrivancyPolicy')}>
                        <Text style={style.navList}>{this.state.language == 'en' ? 'Privacy policy' : 'गोपनीयता नीति'}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={style.list} onPress={() => this.navigateTo('TermCondition')}>
                        <Text style={style.navList}>{this.state.language == 'en' ? 'Term & condition' : 'नियम एवं शर्त'}</Text>
                    </TouchableOpacity>
                    {
                        this.state.user ?
                            <TouchableOpacity style={style.list} onPress={() => this.navigateTo('Feedback')}>
                                <Text style={style.navList}>{this.state.language == 'en' ? 'Feedback' : 'प्रतिपुष्टि'}</Text>
                            </TouchableOpacity>
                            : <></>
                    }
                    <TouchableOpacity style={style.list} onPress={() => this.navigateTo('OtherApps')}>
                        <Text style={style.navList}>{this.state.language == 'en' ? 'Other apps' : 'दूसरे एप्लिकेशन'}</Text>
                    </TouchableOpacity>
                    {
                        this.state.user ?
                            <TouchableOpacity style={style.list} onPress={() => this.logOut()}>
                                <Text style={style.navList}>{this.state.language == 'en' ? 'Logout' : 'लॉग आउट'}</Text>
                            </TouchableOpacity>
                            : <></>
                    }
                </DrawerContentScrollView>
            </>
        )

    }
}

const style = StyleSheet.create({
    navList: {
        paddingHorizontal: 15,
        paddingVertical: 10,
        fontSize: 16,
    },
    list: {
        borderBottomWidth: 0.5,
        borderColor: COLORS.primary,
    }
});