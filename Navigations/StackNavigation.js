import React, { Component } from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

// import HomeScreen from "../Screens/Home/HomeScreen";
import DrawerNavigation from "./DrawerNavigation";
import { AppInfo, COLORS } from "../configs/constants.config";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { ActivityIndicator, Image, Text, View } from "react-native";
import PrivancyScreen from "../src/Pages/PrivancyScreen";
import TermConditionScreen from "../src/Pages/TermConditionScreen";
import NotificationScreen from "../src/Pages/NotificationScreen";
import OtherAppScreen from "../src/Pages/OtherAppScreen";
import VehicleAddScreen from "../src/Vehicle/VehicleAdd";
import ProfileScreen from "../src/Profile/ProfileScreen";
import LoginScreen from "../src/Login/LoginScreen";
import VerifyScreen from "../src/Login/VerifyScreen";
import FeedbackScreen from "../src/Pages/FeedbackScreen";
import SearchVehicleScreen from "../src/Pages/SearchVehicleScreen";
import Login1Screen from "../src/Login/Login1Screen";
import SingleArticleScreen from "../src/Article/SingleArticleScreen";
import SingleVehicleScreen from "../src/Vehicle/SingleVehicleScreen";
import ExpiryVehicleScreen from "../src/Vehicle/ExpiryVehicleScreen";
import DocumentAdd from "../src/Vehicle/DocumentAdd";
import SearchVehicle from "../src/Vehicle/SearchVehicle";
import SingleDocumentScreen from "../src/Vehicle/SingleDocumentScreen";


const Stack = createNativeStackNavigator();

export default class StackNavigation extends Component {
  state = {
    user: null,
    loading: true,
  };
  async componentDidMount() {
    let user = await AsyncStorage.getItem('@user');
    if (user) {
      user = JSON.parse(user);
      console.log('user details', user);
      this.setState({
        user: user,
      });
    }

    setTimeout(() => {
      this.setState({ loading: false })
    }, 5000);
  }

  render() {
    if (this.state.loading == false) {
      return (
        <NavigationContainer>
          <Stack.Navigator
            initialRouteName={
              this.state.user ? 'Home' : 'Login'
            }
            screenOptions={{
              headerTintColor: COLORS.white,
              headerStyle: {
                backgroundColor: COLORS.primary
              }
            }}
          >
            <Stack.Screen
              name="Home"
              component={DrawerNavigation}
              options={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="PrivancyPolicy"
              component={PrivancyScreen}
              options={{
                headerShown: true,
                title: 'Privancy Policy'
              }}
            />
            <Stack.Screen
              name="TermCondition"
              component={TermConditionScreen}
              options={{
                headerShown: true,
                title: 'Term & Condition'
              }}
            />
            <Stack.Screen
              name="Notification"
              component={NotificationScreen}
              options={{
                headerShown: true,
                title: 'Notifications'
              }}
            />
            <Stack.Screen
              name="OtherApps"
              component={OtherAppScreen}
              options={{
                headerShown: true,
                title: 'Other Apps'
              }}
            />
            <Stack.Screen
              name="AddVehicle"
              component={VehicleAddScreen}
              options={{
                headerShown: true,
                title: 'Add Vehicle'
              }}
            />
            <Stack.Screen
              name="Profile"
              component={ProfileScreen}
              options={{
                headerShown: true,
                title: 'Profile'
              }}
            />
            <Stack.Screen
              name="Login"
              component={LoginScreen}
              options={{
                headerShown: false,
                title: ''
              }}
            />
            <Stack.Screen
              name="Login1"
              component={Login1Screen}
              options={{
                headerShown: false,
                title: ''
              }}
            />
            <Stack.Screen
              name="Verify"
              component={VerifyScreen}
              options={{
                headerShown: false,
                title: ''
              }}
            />
            <Stack.Screen
              name="Feedback"
              component={FeedbackScreen}
              options={{
                headerShown: true,
                title: 'Feedback'
              }}
            />
            <Stack.Screen
              name="SearchVehicle"
              component={SearchVehicleScreen}
              options={{
                headerShown: true,
                title: 'Search Vehicle'
              }}
            />
            <Stack.Screen
              name="SingleArticle"
              component={SingleArticleScreen}
              options={{
                headerShown: true,
                title: 'Article'
              }}
            />
            <Stack.Screen
              name="SearchVehicles"
              component={SearchVehicle}
              options={{
                headerShown: true,
                title: 'Search Vehicles'
              }}
            />
            <Stack.Screen
              name="SingleVehicle"
              component={SingleVehicleScreen}
              options={{
                headerShown: true,
                title: 'Vehicle Details'
              }}
            />
            <Stack.Screen
              name="ExpiryVehicle"
              component={ExpiryVehicleScreen}
              options={{
                headerShown: true,
                title: 'Expiry Documents'
              }}
            />
            <Stack.Screen
              name="DocumentAdd"
              component={DocumentAdd}
              options={{
                headerShown: true,
                title: 'Add Document'
              }}
            />
            <Stack.Screen
              name="SingleDocument"
              component={SingleDocumentScreen}
              options={{
                headerShown: true,
                title: 'Licence Details'
              }}
            />

          </Stack.Navigator>
        </NavigationContainer>
      );
    } else {
      return (
        <View style={{ backgroundColor: COLORS.primary, position: 'absolute', top: 0, bottom: 0, left: 0, right: 0, justifyContent: 'center', alignItems: 'center' }}>
          {/* <ActivityIndicator size="large" color={COLORS.white} /> */}

          {/* <Image
            source={require('../assets/logo.png')}
            resizeMode="contain"
            style={{ height: 130 }}
          /> */}
          <Text style={{ color: COLORS.white, fontSize: 18 }}>{AppInfo.name}</Text>
        </View>
      );
    }
  }
}