import React, { Component } from "react";
import { createDrawerNavigator } from '@react-navigation/drawer';
import DrawerContent from "./DrawerContent";
import { Switch, Text, View } from "react-native";
import { AppInfo, COLORS } from "../configs/constants.config";
import HomeScreen from "../src/Home/HomeScreen";
import { Icon } from "react-native-elements";
import SwitchWithIcons from "react-native-switch-with-icons";
import darkMode from "../assets/dark-mode.png";
import lightMode from "../assets/light-mode.png";
import NavTab from "./NavTab";
import AsyncStorage from "@react-native-async-storage/async-storage";
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
  MenuProvider
} from 'react-native-popup-menu';
const Drawer = createDrawerNavigator();

export default class DrawerNavigation extends Component {
  state = {
    isEnabled: true,
    user: null,
    language: null,
    mode: null,
  };
  async componentDidMount() {
    let user = await AsyncStorage.getItem('@user');
    if (user) {
      user = JSON.parse(user);
      console.log('user details', user);
      this.setState({
        user: user,
      });
    }
    this.storageData();
  }
  storageData = async () => {
    let language = await AsyncStorage.getItem('@language');
    if (language) {
      this.setState({ language: language });
    } else {
      this.setState({ language: 'en' });
    }
    console.log('language', language);
    let mode = await AsyncStorage.getItem('@mode');
    if (mode) {
      this.setState({ mode: mode });
    } else {
      this.setState({ mode: 'light' });
    }
    console.log('mode', mode);
  }
  setLanguage = async (language) => {
    console.log('set language', language);
    await AsyncStorage.setItem('@language', language);

    this.storageData();
  }
  setMode = async (mode) => {
    console.log('set mode', mode);
    await AsyncStorage.setItem('@mode', mode);

    this.storageData();
  }
  toggleSwitch = (status) => {
    this.setState({ isEnabled: status });
  }
  navigateTo = (path, params = null) => {
    let { navigation } = this.props;

    navigation.navigate(path, params);
  }
  render() {
    const switchIcon = {
      true: lightMode, false: darkMode
    };
    let username = 'Jhon Due';
    if (this.state.user) {
      if (this.state.user.first_name && this.state.user.first_name != '') {
        username = this.state.user.first_name + ' ' + this.state.user.last_name;
      } else {
        username = this.state.user.mobile;
      }
    }
    return (
      <Drawer.Navigator
        drawerContent={() => <DrawerContent {...this.props} />}
        screenOptions={{
          headerStyle: {
            backgroundColor: COLORS.primary,
          },
          headerTintColor: COLORS.white
        }}
      >
        <Drawer.Screen
          name="NavTab"
          component={NavTab}

          options={{
            // title: AppInfo.name,
            title: `${this.state.language && this.state.language == 'en' ? 'Hello' : 'नमस्ते'}, ${username}`,
            headerRight: () =>
              <View>
                <Text>
                  <Icon
                    name='notifications-outline'
                    type='ionicon'
                    color='#fff'
                    style={{ marginRight: 20 }}
                    onPress={() => this.navigateTo('Notification')}
                  />
                  <View style={{ width: 20 }}>
                  </View>
                  <Menu >
                    <MenuTrigger>
                      <Icon
                        name='dots-three-vertical'
                        type='entypo'
                        color='#fff'
                        size={20}
                        style={{ paddingRight: 10, paddingBottom: 2, fontSize: 10 }}
                      />
                    </MenuTrigger>
                    <MenuOptions style={{ width: 150, backgroundColor: '#fff', position: 'absolute', top: 30, right: 15, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.8, shadowRadius: 2, elevation: 5, }}>
                      <MenuOption onSelect={() => { this.state.language == 'en' ? this.setLanguage('hi') : this.setLanguage('en') }} style={{ flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#ccc', paddingHorizontal: 10 }}>
                        <Text style={{ fontSize: 17 }}>{this.state.language == 'en' ? 'Language' : 'भाषा'}</Text>
                        <View>
                          {
                            this.state.language == 'hi' ?
                              <Text style={{ borderWidth: 1, borderColor: COLORS.primary, marginLeft: 30, paddingHorizontal: 5, paddingVertical: 2, backgroundColor: COLORS.primary, color: COLORS.white }}>Hi</Text>
                              :
                              <Text style={{ borderWidth: 1, borderColor: COLORS.primary, marginLeft: 30, paddingHorizontal: 5, paddingVertical: 2, backgroundColor: COLORS.primary, color: COLORS.white }}>En</Text>
                          }
                        </View>
                      </MenuOption>
                      <MenuOption onSelect={() => { this.state.mode == 'light' ? this.setMode('dark') : this.setMode('light') }} style={{ flexDirection: 'row', paddingHorizontal: 10 }}>
                        <Text style={{ fontSize: 17 }}>{this.state.language == 'en' ? 'Mode' : 'मोड'}</Text>
                        {
                          this.state.mode == 'light' ?
                            <Icon name="contrast-outline" type='ionicon' style={{ textAlign: 'right', paddingLeft: 63 }} />
                            :
                            <Icon name="contrast" type='ionicon' style={{ textAlign: 'right', paddingLeft: 63 }} />
                        }
                      </MenuOption>
                    </MenuOptions>
                  </Menu>
                  {/* <SwitchWithIcons
                    trackColor={{ false: "#4d4d4d", true: "#cfcfcf" }}
                    thumbColor={this.state.isEnabled ? "#000" : "#fff"}
                    iconColor={this.state.isEnabled ? "#fff" : "#fff"}
                    ios_backgroundColor="#3e3e3e"
                    icon={switchIcon}
                    onValueChange={(status) => this.toggleSwitch(status)}
                    value={this.state.isEnabled}
                  /> */}
                </Text>
              </View >
          }
          }
        />
      </Drawer.Navigator >
    );
  }
}