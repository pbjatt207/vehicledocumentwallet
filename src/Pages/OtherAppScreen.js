import React, { Component } from "react";
import { ActivityIndicator, Image, Linking, ScrollView, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { commanApi } from "../../Api/api";
import { COLORS, SCREEN } from "../../configs/constants.config";

export default class OtherAppScreen extends Component {
    state = {
        apps: null,
        loading: false,
    };

    async componentDidMount() {
        this.otherappApi();
    }

    otherappApi = async () => {
        let { language } = this.state;
        this.setState({ loading: true });
        let response = await commanApi(`other_app`);
        if (response.status) {
            if (response.data.status) {
                console.log('response', response.data.data);
                this.setState({ apps: response.data.data });
                setTimeout(() => {
                    this.setState({ loading: false });
                }, 30);
            } else {
                this.setState({ loading: false });
                Alert.alert(
                    "Error",
                    response.data.message,
                    [
                        { text: "OK", onPress: () => console.log("OK Pressed") }
                    ]
                );
            }
        }
    }
    render() {
        return (
            <>
                {
                    this.state.loading ?
                        <ActivityIndicator size="large" color={COLORS.primary} style={{ marginTop: 30 }} />
                        :
                        <ScrollView>
                            <View style={{ margin: 15 }}>
                                <View style={style.Row}>
                                    {
                                        this.state.apps && this.state.apps.length && this.state.apps.map((app, i) => {
                                            return (
                                                <TouchableOpacity onPress={() => Linking.openURL(app.url)} style={style.Column}>
                                                    
                                                    <Image
                                                        source={{ uri: app.image_url }}
                                                        resizeMode="contain"
                                                        style={{ height: 100, width:'100%' }}
                                                    />
                                                    
                                                    
                                                </TouchableOpacity>
                                            )
                                        })
                                    }
                                </View>
                            </View>
                        </ScrollView>
                }
            </>
        );
    }
}
const style = StyleSheet.create({
    Row: {
        flexDirection: 'row',
        // justifyContent: 'center',
        flexWrap: 'wrap'
    },
    Column: {
        width: (SCREEN.width - 70) / 2 - 10 / 2,
        alignItems: 'center',
        margin: 10,
        backgroundColor: '#ccc',
        borderRadius: 8, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.8, shadowRadius: 2, elevation: 5,
    }
});