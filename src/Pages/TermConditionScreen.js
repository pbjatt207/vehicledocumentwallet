import React, { Component } from "react";
import { ActivityIndicator, ScrollView, Text, View } from "react-native";
import { commanApi } from "../../Api/api";
import { COLORS } from "../../configs/constants.config";

export default class TermConditionScreen extends Component {
    state = {
        language: 'en',
        privancy: null,
        loading: false,
    };

    async componentDidMount() {
        this.privancyApi();
    }

    privancyApi = async () => {
        let { language } = this.state;
        this.setState({ loading: true });
        let response = await commanApi(`page/term_condition?language=${language}`);
        
        if (response.status) {
            if (response.data.status) {
                console.log('response', response.data.data);
                this.setState({ privancy: response.data.data });
                setTimeout(() => {
                    this.setState({ loading: false });
                }, 30);
            } else {
                this.setState({ loading: false });
                Alert.alert(
                    "Error",
                    response.data.message,
                    [
                        { text: "OK", onPress: () => console.log("OK Pressed") }
                    ]
                );
            }
        }
    }
    render() {
        console.log('term',this.state.privancy);
        return (
            <>
                {
                    this.state.loading ?
                        <ActivityIndicator size="large" color={COLORS.primary} style={{ marginTop: 30 }} />
                        :
                        <ScrollView>
                            <View style={{ marginLeft: 15, marginRight: 15, marginBottom: 15, }}>
                                {
                                    this.state.privancy && this.state.privancy.description ?
                                        <Text style={{ marginTop: 10, textAlign: "justify" }}>
                                            {this.state.privancy.description}
                                        </Text>
                                        :
                                        <Text style={{ textAlign: 'center', marginTop: 10, }}>No records found.</Text>
                                }
                            </View>
                        </ScrollView>
                }
            </>
        );
    }
}