import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { ActivityIndicator, Alert, Button, Image, Pressable, ScrollView, Text, TextInput, ToastAndroid, TouchableOpacity, View } from "react-native";
import { Icon } from "react-native-elements";
import Svg, { Path } from "react-native-svg";
import { sendOtp } from "../../Api/api";
import { COLORS, SCREEN } from "../../configs/constants.config";

export default class Login1Screen extends Component {
    state = {
        mobile: '',
        loading: false,
    }
    componentDidMount = () => {

        // ToastAndroid.showWithGravityAndOffset(
        //     "A wild toast appeared!",
        //     ToastAndroid.LONG,
        //     ToastAndroid.BOTTOM,
        //     25,
        //     50
        // );
    }
    VehicleNumber = (number) => {
        this.setState({ mobile: number });
    }
    navigateTo = (path, params = null) => {
        let { navigation } = this.props;
        navigation.navigate(path, params);
    }
    Login = async () => {
        let { mobile } = this.state;
        if (!mobile && mobile == '') {
            // ToastAndroid.show("please enter mobile number !", ToastAndroid.SHORT);
            ToastAndroid.show("please enter mobile number !", ToastAndroid.SHORT);
            // Alert.alert(
            //     "Error",
            //     'please enter mobile number.',
            //     [
            //         { text: "OK", onPress: () => this.inputFocus.focus() }
            //     ]
            // );
            console.log('please enter mobile number !');
            this.inputFocus.focus();
        } else {
            this.setState({ loading: true });
            let response = await sendOtp(`sendOtp?mobile=${mobile}`);
            if (response.status) {
                if (response.data.status) {
                    console.log('response1', response.data);
                    // await AsyncStorage.setItem('@user', JSON.stringify(response.data.data));
                    setTimeout(() => {
                        this.navigateTo('Verify', response.data.data);
                        this.setState({ loading: false });
                        this.setState({ mobile: '' });
                    }, 30);
                } else {
                    this.setState({ loading: false });
                    Alert.alert(
                        "Error",
                        response.data.response_message,
                        [
                            { text: "OK", onPress: () => console.log("OK Pressed") }
                        ]
                    );
                }
            }
        }
    }
    render() {
        return (
            <>
                <View style={{ backgroundColor: COLORS.primary, height: SCREEN.height }}>
                    <View style={{ backgroundColor: '#f2f2f2' }}>
                        <View style={{ width: 110, height: 110, justifyContent: 'center', marginTop: 50, alignSelf: 'center' }}>
                            <Image
                                source={require('../../assets/logo.png')}
                                resizeMode="contain"
                                style={{ width: 80, height: 80, alignSelf: 'center' }}
                            />
                        </View>
                        <Svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320" style={{ width: SCREEN.width + 5, height: SCREEN.height * 2 / 9, marginBottom: -40 }}>
                            <Path fill={COLORS.primary} fill-opacity="1" d="M0,256L48,234.7C96,213,192,171,288,170.7C384,171,480,213,576,240C672,267,768,277,864,240C960,203,1056,117,1152,80C1248,43,1344,53,1392,58.7L1440,64L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"></Path>
                        </Svg>
                    </View>
                    <View style={{ marginLeft: 20, marginRight: 20, marginBottom: 15, justifyContent: 'center', marginTop: '10%' }}>
                        <Text style={{ fontSize: 25, textAlign: 'center', color: COLORS.white, marginTop: 50, marginBottom: 20, fontWeight: 'bold' }}>LOGIN / REGISTER</Text>
                        <TextInput
                            style={{ marginTop: 10, backgroundColor: COLORS.white, height: 40, fontSize: 15, borderRadius: 10, paddingHorizontal: 20 }}
                            onChangeText={number => this.VehicleNumber(number)}
                            value={this.state.mobile}
                            placeholder="Mobile Number"
                            maxLength={10}
                            ref={ref => this.inputFocus = ref}
                            keyboardType="numeric"
                        />
                        {
                            this.state.loading ?
                                <View style={{ marginTop: 10, backgroundColor: '#ccc', height: 40, fontSize: 15, alignSelf: 'center', borderRadius: 10, width: '100%', paddingHorizontal: 20, marginTop: 30, justifyContent: 'center' }}>
                                    <ActivityIndicator size="small" color={COLORS.white} />
                                </View>
                                :
                                <TouchableOpacity onPress={() => this.Login()} style={{ marginTop: 10, backgroundColor: '#ccc', height: 40, fontSize: 15, alignSelf: 'center', borderRadius: 10, width: '100%', paddingHorizontal: 20, marginTop: 30, justifyContent: 'center' }}>
                                    <Text style={{ textAlign: 'center', fontSize: 19, color: COLORS.white, fontWeight: 'bold' }}>SUBMIT</Text>
                                </TouchableOpacity>
                        }
                    </View>
                </View>
            </>
        );
    }
}