import { Picker } from "@react-native-picker/picker";
import moment from "moment";
import React, { Component } from "react";
import { ActivityIndicator, Alert, AsyncStorage, Button, Image, Modal, Pressable, ScrollView, StyleSheet, Text, ToastAndroid, TouchableOpacity, View } from "react-native";
// import DatePicker from "react-native-date-picker";
import DatePicker from 'react-native-datepicker';
import { Icon } from "react-native-elements";
import { launchCamera, launchImageLibrary } from "react-native-image-picker";
import { TextInput } from "react-native-paper";
import { addLicense, addVehicle, commanApi } from "../../Api/api";
import { COLORS } from "../../configs/constants.config";

export default class DocumentAdd extends Component {
    state = {
        user: null,
        category_id: null,
        reg_number: '',
        date: new Date(),
        name: '',
        image: null,
        documentArr: [],
        open: false,
        categories: [],
        documents: [],
        modalVisible: false,
        selectedIndex: null
    };
    VehicleNumber = (reg_number) => {
        console.log(reg_number.toUpperCase());
        this.setState({ reg_number: reg_number.toUpperCase() });
    }
    setOpen = async (status) => {
        console.log(status);
        this.setState({ open: status });
    }
    setDate = async (date) => {
        console.log('date', date);
        this.setState({ date: date });
    }
    navigateTo = (path, params = null) => {
        let { navigation } = this.props;

        navigation.navigate(path, params);
    }



    handleChoosePhoto(type = 'camera', field = 'image') {
        console.log('field', this.state.field);
        let self = this
        this.setState({ modalVisible: false })
        const options = {
            noData: true,
        };
        type === 'camera' ?
            launchCamera(options, (response) => {

                console.log('res', response);
                if (response.assets && response.assets[0].uri) {
                    self.setState({ [this.state.field]: response.assets[0] });
                }
            })
            : launchImageLibrary(options, (response) => {
                // if (response.assets[0].uri) {
                //         self.setState({ [this.state.field]: response.assets[0] });
                // }
                if (response.assets && response.assets[0].uri) {
                    self.setState({ [this.state.field]: response.assets[0] });
                }
            });
    }



    saveLicense = async () => {
        let { name, user, date, image } = this.state;

        this.setState({ loading: true });
        let data = {
            name: name,
            user_id: user.id,
            expiry_date: moment.utc(date).format('YYYY-MM-DD'),
        }
        data = this.createFormData(image, data);

        let response = await addLicense(data);
        console.log('data', response);
        if (response.status) {
            if (response.data.status) {
                setTimeout(() => {
                    this.setState({ loading: false });
                    this.setState({ name: '' });
                    this.setState({ image: '' });
                    ToastAndroid.show("Your License Add Successfully !", ToastAndroid.SHORT);
                }, 30);
            } else {
                this.setState({ loading: false });
                Alert.alert(
                    "Error",
                    response.data.response_message,
                    [
                        { text: "OK", onPress: () => console.log("OK Pressed") }
                    ]
                );
            }
        }
    }
    createFormData = (image, body = {}) => {
        const data = new FormData();

        if (image) {
            data.append('image', {
                name: image.fileName,
                type: image.type,
                uri: Platform.OS === 'ios' ? image.uri.replace('file://', '') : image.uri,
            });
        }

        Object.keys(body).forEach((key) => {
            data.append(key, body[key]);
        });

        return data;
    }

    async componentDidMount() {
        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            console.log('user details', user);
            this.setState({ user: user });
        }
    }

    render() {
        console.log('date', this.state.date);
        console.log('image: ', this.state.image);
        return (
            <>
                <ScrollView>
                    <View style={{ marginLeft: 15, marginRight: 15, marginBottom: 15 }}>
                        <View>
                            <Text style={{ marginTop: 10, color: '#000', fontSize: 17, fontWeight: 'bold' }}>Document Details</Text>
                            <TextInput
                                label="Name"
                                activeUnderlineColor={COLORS.primary}
                                style={{ marginTop: 10, backgroundColor: 'transparent', height: 50, fontSize: 15 }}
                                onChangeText={name => this.setState({ name })}
                                value={this.state.name}
                            />

                            <View style={{ flexDirection: 'row', paddingBottom: 20 }}>
                                <View style={{ flex: .6 }}>
                                    <DatePicker
                                        style={{ width: '100%', marginVertical: 10, borderRadius: 5, backgroundColor: '#fff', }}
                                        date={this.state.date} // Initial date from state
                                        value={this.state.date}
                                        mode="date" // The enum of date, datetime and time
                                        placeholder="select date"
                                        format="YYYY-MM-DD"
                                        // minDate="01-01-2016"
                                        // maxDate="01-01-2019"
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        customStyles={{
                                            dateIcon: {
                                                //display: 'none',
                                                position: 'absolute',
                                                right: 0,
                                                top: 4,
                                                marginRight: 0,
                                            },
                                            dateInput: {
                                                marginRight: 36,
                                                borderRadius: 5,
                                                borderColor: '#00afef'
                                            },
                                        }}
                                        onDateChange={date => this.setState({ date })}
                                    />
                                </View>
                                <View style={{ flex: .4, alignItems: 'center', justifyContent: 'center' }}>
                                    <TouchableOpacity onPress={() => this.setState({ modalVisible: true, field: 'image' })} style={{ borderStyle: 'dotted', borderWidth: 1, paddingHorizontal: 10 }}>


                                        <Icon
                                            name='add'
                                            type='ionicon'
                                            color='#000'
                                            size={30}
                                            style={{ alignSelf: 'center' }}
                                        />
                                        <Text>Add File</Text>

                                    </TouchableOpacity>
                                </View>
                            </View>
                            {
                                this.state.name ?
                                    this.state.loading
                                        ?

                                        <TouchableOpacity style={{ marginTop: 10, backgroundColor: COLORS.primary, height: 40, fontSize: 15, alignSelf: 'center', borderRadius: 4, width: '100%', paddingHorizontal: 15, marginTop: 30, justifyContent: 'center' }}>
                                            <ActivityIndicator size="small" color={COLORS.white} />
                                        </TouchableOpacity>
                                        :
                                        <TouchableOpacity onPress={() => this.saveLicense()} style={{ marginTop: 10, backgroundColor: COLORS.primary, height: 40, fontSize: 15, alignSelf: 'center', borderRadius: 4, width: '100%', paddingHorizontal: 15, marginTop: 30, justifyContent: 'center' }}>
                                            <Text style={{ textAlign: 'center', fontSize: 19, color: COLORS.white, fontWeight: 'bold' }}>ADD</Text>
                                        </TouchableOpacity>
                                    :
                                    <View style={{ marginTop: 10, backgroundColor: COLORS.primary_fade, height: 40, fontSize: 15, alignSelf: 'center', borderRadius: 4, width: '100%', paddingHorizontal: 15, marginTop: 30, justifyContent: 'center' }}>
                                        <Text style={{ textAlign: 'center', fontSize: 19, color: COLORS.white, fontWeight: 'bold' }}>ADD</Text>
                                    </View>
                            }

                            <Modal
                                animationType="slide"
                                transparent={true}
                                visible={this.state.modalVisible}
                                onRequestClose={() => {
                                    Alert.alert("Modal has been closed.");
                                    this.setState({ modalVisible: false });
                                }}
                            >
                                <View style={styles.centeredView}>
                                    <View style={styles.modalView}>
                                        <TouchableOpacity onPress={() => this.handleChoosePhoto('camera', this.state.field)}>
                                            <Text style={styles.modalText}>Take Photo...</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.handleChoosePhoto('library')}>
                                            <Text style={styles.modalText}>Choose From library...</Text>
                                        </TouchableOpacity>

                                        <TouchableOpacity
                                            onPress={() => this.setState({ modalVisible: false })}
                                        >
                                            <Text style={styles.textStyle}>Cancel</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </Modal>

                            {/* <Text style={{ marginTop: 10, color: COLORS.primary, fontSize: 17 }}>Permit</Text>
                            <View style={{ flexDirection: 'row' }}>
                                <TouchableOpacity onPress={() => this.setOpen(true)} style={{ flex: .6 }}>
                                    <TextInput
                                        dense={true}
                                        label="Expiry Date"
                                        activeUnderlineColor={COLORS.primary}
                                        style={{ backgroundColor: 'transparent', height: 50, fontSize: 15 }}
                                        value={moment(this.state.date).format('D MMMM Y')}
                                    />
                                </TouchableOpacity>
                                <DatePicker
                                    modal
                                    mode="date"
                                    open={this.state.open}
                                    date={this.state.date}
                                    onConfirm={(date) => {
                                        this.setOpen(false)
                                        this.setDate(date)
                                    }}
                                    onCancel={() => {
                                        this.setOpen(false)
                                    }}
                                />
                                <View style={{ flex: .4, alignItems: 'center', justifyContent: 'center' }}>
                                    <View style={{ borderStyle: 'dotted', borderWidth: 1, paddingHorizontal: 10 }}>
                                        <Icon
                                            name='add'
                                            type='ionicon'
                                            color='#000'
                                            size={30}
                                            style={{ alignSelf: 'center' }}
                                        />
                                        <Text>Add File</Text>
                                    </View>
                                </View>
                            </View> */}
                        </View>

                        {/* <View>
                            <Text style={{ marginTop: 10, color: COLORS.primary, fontWeight: 'bold', fontSize: 16 }}>Private Car</Text>
                            <View style={{ marginTop: 15, backgroundColor: COLORS.white, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.8, shadowRadius: 2, elevation: 5, borderRadius: 8 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <View style={{ flex: 0.2, justifyContent: 'center', paddingTop: 10, paddingBottom: 10 }}>
                                        <Icon
                                            name='car'
                                            type='antdesign'
                                            color='#000'
                                            size={32}
                                        />
                                    </View>
                                    <View style={{ flex: 0.8, paddingTop: 10, paddingBottom: 10 }}>
                                        <Text>RJ19AB0001</Text>
                                        <Text style={{ fontSize: 11 }}>Added 1 week ago</Text>
                                    </View>
                                </View>
                            </View>
                        </View> */}
                    </View>
                </ScrollView>
            </>
        );
    }
}

export const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 0,
        backgroundColor: 'rgba(52, 52, 52, .3)',

    },
    modalView: {
        margin: 0,
        width: 300,
        backgroundColor: "white",
        borderRadius: 5,
        padding: 35,
        // alignItems: "left",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    buttonOpen: {
        backgroundColor: "#F194FF",
    },
    buttonClose: {
        backgroundColor: "#2196F3",
    },
    textStyle: {
        color: "#000",
        fontWeight: "bold",
        textAlign: "right",
        fontSize: 17,
        textTransform: 'uppercase'
    },
    modalText: {
        marginBottom: 15,
        paddingVertical: 8,
        fontSize: 17
    },
})