import moment from "moment";
import React, { Component } from "react";
import { ActivityIndicator, Alert, Image, ScrollView, Text, View } from "react-native";
import { commanApi } from "../../Api/api";
import { COLORS } from "../../configs/constants.config";

export default class SingleDocumentScreen extends Component {
    state = {
        loading: false,
        document: ''
    }
    async componentDidMount() {
        let { route } = this.props;
        this.setState({ document: route.params });
    }
    render() {
        let document = this.state.document;
        return (
            <>
                {
                    this.state.loading ?
                        <ActivityIndicator size="large" color={COLORS.primary} style={{ marginTop: 30 }} />
                        :
                        <ScrollView>
                            <View style={{ paddingHorizontal: 20, paddingTop: 10, paddingBottom: 10 }}>
                                <View style={{ flexDirection: 'row', paddingVertical: 10 }}>
                                    <Text style={{ flex: 5 }}>Name :</Text>
                                    <Text style={{ flex: 5, textTransform: 'uppercase' }}>{document.name}</Text>
                                </View>
                                <View style={{ flexDirection: 'row', paddingVertical: 10 }}>
                                    <Text style={{ flex: 5 }}>Expiry Date :</Text>
                                    <Text style={{ flex: 5 }}>{moment(document.expiry_date).format('DD MMM YYYY')}</Text>
                                </View>
                                {(() => {
                                    if (document.image != '') {
                                        return (
                                            <>
                                                <View>
                                                    <Text>File : </Text>
                                                </View>
                                                <View style={{ borderWidth: .8, borderColor: '#ccc', marginVertical: 10 }}>
                                                    <Image source={{ uri: document.image_url }} style={{ height: 150, resizeMode: 'center' }} />
                                                </View>
                                            </>
                                        )
                                    }
                                    return null;
                                })()}
                            </View>
                        </ScrollView>
                }
            </>
        );
    }
}