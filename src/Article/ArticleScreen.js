import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { ActivityIndicator, Image, ScrollView, Text, TouchableOpacity, View } from "react-native";
import { Icon } from "react-native-elements";
import { commanApi } from "../../Api/api";
import { COLORS, SCREEN } from "../../configs/constants.config";

export default class ArticleScreen extends Component {

    state = {
        loading: false,
        language: 'en',
        articles: []
    }

    ArticleApi = async () => {
        let { language } = this.state;
        let url = `article_list?language=${language}`
        this.setState({ loading: true });
        let response = await commanApi(url);
        console.log('res', response);
        if (response.status) {
            if (response.data.status) {
                this.setState({ articles: response.data.data });
                setTimeout(() => {
                    this.setState({ loading: false });
                }, 30);
            } else {
                this.setState({ loading: false });
                Alert.alert(
                    "Error",
                    response.data.message,
                    [
                        { text: "OK", onPress: () => console.log("OK Pressed") }
                    ]
                );
            }
        }
    }
    async componentDidMount() {
        let lang = await AsyncStorage.getItem('@language');
        if (lang) {
            this.setState({ language: lang });
        }
        this.ArticleApi();
    }

    navigateTo = (path, params = null) => {
        let { navigation } = this.props;
        navigation.navigate(path, params);
    }

    render() {
        return (
            <>
                {
                    this.state.loading ?
                        <ActivityIndicator size="large" color={COLORS.primary} style={{ marginTop: 30 }} />
                        :
                        <ScrollView>
                            <View style={{ marginLeft: 15, marginRight: 15, marginBottom: 15 }}>
                                {
                                    this.state.articles.map((item, index) => {
                                        return (
                                            <View style={{ marginTop: 15, backgroundColor: COLORS.white, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.8, shadowRadius: 2, elevation: 5, borderRadius: 8 }}>
                                                <View>
                                                    <View style={{ justifyContent: 'center', paddingTop: 10, paddingBottom: 10 }}>
                                                        <Image
                                                            source={{ uri: item.image_url }}
                                                            resizeMode="contain"
                                                            style={{
                                                                width: '100%',
                                                                height: 150
                                                            }} />
                                                    </View>
                                                    <View style={{ paddingRight: 10, paddingLeft: 10, paddingBottom: 10 }}>
                                                        <Text style={{ fontWeight: 'bold', fontSize: 17 }}>{item.name}</Text>
                                                        <Text style={{ fontSize: 15 }} numberOfLines={3} ellipsizeMode="tail">{item.description}</Text>
                                                    </View>
                                                    <TouchableOpacity onPress={() => this.navigateTo('SingleArticle', item)} style={{ paddingHorizontal: 10, paddingVertical: 5, backgroundColor: COLORS.primary, borderBottomRightRadius: 8, borderBottomLeftRadius: 8 }}>
                                                        <View style={{ alignItems: 'flex-end', flexDirection: 'row', justifyContent: 'center', paddingVertical: 5 }}>
                                                            <Text style={{ color: COLORS.white, fontSize: 17 }}>Read More</Text>
                                                            {/* <Icon name='arrow-forward-outline' type='ionicon' color={COLORS.white} /> */}
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        );
                                    })
                                }
                            </View>
                        </ScrollView >
                }
            </>
        );
    }
}