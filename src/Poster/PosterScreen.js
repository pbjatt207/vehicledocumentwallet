import React, { Component } from "react";
import { ActivityIndicator, FlatList, Image, Pressable, ScrollView, Text, TouchableOpacity, View } from "react-native";
import { Icon } from "react-native-elements";
import { COLORS, SCREEN } from "../../configs/constants.config";
import ImageView from 'react-native-image-view';
import { commanApi } from "../../Api/api";

export default class PosterScreen extends Component {
    state = {
        loading: false,
        isImageViewVisible: false,
        images: [],
        imageIndex: 0,
        posters: []
    }

    PosterApi = async () => {
        let url = `poster_list`
        this.setState({ loading: true });
        let response = await commanApi(url);
        console.log('res', response);
        if (response.status) {
            if (response.data.status) {
                this.setState({ posters: response.data.data });

                let images = [];

                this.state.posters.forEach(p => {
                    images.push({
                        source: {
                            uri: p.image_url
                        }
                    });
                });

                this.setState({ images });
                setTimeout(() => {
                    this.setState({ loading: false });
                }, 30);
            } else {
                this.setState({ loading: false });
                Alert.alert(
                    "Error",
                    response.data.message,
                    [
                        { text: "OK", onPress: () => console.log("OK Pressed") }
                    ]
                );
            }
        }
    }

    async componentDidMount() {

        this.PosterApi();
    }

    changeImage = async (index = null) => {
        console.log('index : ', index);
        if (index) {
            this.setState({ imageIndex: index });
        }
        if (this.state.isImageViewVisible) {
            this.setState({ isImageViewVisible: false });
        } else {
            this.setState({ isImageViewVisible: true });
        }
    }
    render() {
        return (
            <>
                {
                    this.state.loading ?
                        <ActivityIndicator size="large" color={COLORS.primary} style={{ marginTop: 30 }} />
                        :
                        <ScrollView>
                            <View style={{ marginLeft: 15, marginRight: 15, marginBottom: 15 }}>
                                {/* <Text style={{ marginTop: 10, color: COLORS.primary, fontWeight: 'bold', fontSize: 16 }}>Private Car</Text> */}
                                <View style={{ marginTop: 10 }}>
                                    <ImageView
                                        images={this.state.images}
                                        onClose={() => this.changeImage()}
                                        imageIndex={this.state.imageIndex}
                                        animationType={'fade'}
                                        isVisible={this.state.isImageViewVisible}
                                        isSwipeCloseEnabled={true}
                                        isPinchZoomEnabled={true}
                                        renderFooter={() => (
                                            <Pressable style={{ padding: 0, backgroundColor: COLORS.primary, borderRadius: 50, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.8, shadowRadius: 2, elevation: 5, width: 45, alignItems: 'center', justifyContent: 'center', height: 45, zIndex: 999, position: 'absolute', bottom: 35, left: '45%' }}>
                                                <Icon name="share-social-outline" type='ionicon' />
                                            </Pressable>
                                        )}
                                    />
                                    <FlatList
                                        data={this.state.posters}
                                        numColumns={2}
                                        renderItem={({ item, index }) => (
                                            <TouchableOpacity
                                                onPress={() => this.changeImage(index)}
                                                style={{
                                                    margin: 10,
                                                    flex: 1,
                                                    flexDirection: 'column',
                                                    justifyContent: 'space-evenly',
                                                    flex: 1,
                                                    backgroundColor: '#fff',
                                                    overflow: 'hidden',
                                                    shadowColor: "#000",
                                                    shadowOffset: {
                                                        width: 0,
                                                        height: 1,
                                                    },
                                                    shadowOpacity: 0.20,
                                                    shadowRadius: 1.41,
                                                    elevation: 2,
                                                }}>
                                                <View style={{ alignItems: 'center' }}>
                                                    <Image
                                                        source={{ uri: item.image_url }}
                                                        resizeMode="contain"
                                                        style={{
                                                            width: SCREEN.width / 2.1 - 11,
                                                            height: 180
                                                        }} />
                                                </View>
                                            </TouchableOpacity>

                                        )}
                                        //Setting the number of column

                                        keyExtractor={(item, index) => index}
                                    />
                                </View>
                            </View>
                        </ScrollView>
                }
            </>
        );
    }
}