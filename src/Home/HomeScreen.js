import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { ActivityIndicator, Alert, Button, Image, Linking, SafeAreaView, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from "react-native";
import { Menu } from "react-native-paper";
import Carousel from "react-native-snap-carousel";
import { commanApi, searchApi } from "../../Api/api";
import { COLORS, SCREEN } from "../../configs/constants.config";

export default class HomeScreen extends Component {
    state = {
        vehicleId: null,
        language: 'en',
        user: null,
        home: null,
        loading: true,
        activeIndex: 0,
        carouselItems: [
            {
                image: "https://jssors8.azureedge.net/demos/image-slider/img/px-beach-daylight-fun-1430675-image.jpg",
                text: "Text 1",
            },
            {
                image: "https://jssors8.azureedge.net/demos/image-slider/img/px-beach-daylight-fun-1430675-image.jpg",
                text: "Text 1",
            },
            {
                image: "https://jssors8.azureedge.net/demos/image-slider/img/px-beach-daylight-fun-1430675-image.jpg",
                text: "Text 1",
            },
            {
                image: "https://jssors8.azureedge.net/demos/image-slider/img/px-beach-daylight-fun-1430675-image.jpg",
                text: "Text 1",
            },
            {
                image: "https://jssors8.azureedge.net/demos/image-slider/img/px-beach-daylight-fun-1430675-image.jpg",
                text: "Text 1",
            },
        ]
    };

    VehicleNumber = (vehicleId) => {
        this.setState({ vehicleId: vehicleId.toUpperCase().replace(/\s/g, '') });
    }
    async componentDidMount() {
        let user = await AsyncStorage.getItem('@user');
        let lang = await AsyncStorage.getItem('@language');
        if (lang) {
            this.setState({ language: lang });
        }
        if (user) {
            user = JSON.parse(user);
            console.log('user details', user);
            this.setState({ user: user });
        }
        this.HomeApi();
    }
    navigateTo = (path, params = null) => {
        let { navigation } = this.props;
        navigation.navigate(path, params);
    }
    HomeApi = async () => {
        let { language, user } = this.state;
        let url;
        if (user && user.id) {
            // url = `home?language=${language}&user_id=${user.id}`
            url = `home?language=en&user_id=${user.id}`
        } else {
            // url = `home?language=${language}`
            url = `home?language=en`
        }
        this.setState({ loading: true });
        let response = await commanApi(url);

        if (response.status) {
            if (response.data.status) {
                this.setState({ home: response.data.data });
                setTimeout(() => {
                    this.setState({ loading: false });
                }, 30);
            } else {
                this.setState({ loading: false });
                Alert.alert(
                    "Error",
                    response.data.message,
                    [
                        { text: "OK", onPress: () => console.log("OK Pressed") }
                    ]
                );
            }
        }
    }
    SearchApi = async () => {
        let { language, user } = this.state;
        let url = `home?language=${language}`;
        this.setState({ loading: true });
        let data = {
            vehicleId: this.state.vehicleId
        }

        let response = await searchApi(data);
        console.log('response1', response);
        if (response.status) {
            if (response.data.status != 'error') {

                // await AsyncStorage.setItem('@user', JSON.stringify(response.data.data));
                setTimeout(() => {
                    this.navigateTo('SearchVehicle', response.data.response);
                    this.setState({ loading: false });
                }, 30);
            } else {
                this.setState({ loading: false });
                Alert.alert(
                    "Error",
                    response.data.response,
                    [
                        { text: "OK", onPress: () => console.log("OK Pressed") }
                    ]
                );
            }
        }
    }
    redirectExpiryVehicle = async (name) => {
        this.state.home.documents.forEach(doc => {
            if (doc.name == name) {
                this.navigateTo('ExpiryVehicle', doc);
            }
        })
    }
    render() {
        return (
            <>
                {
                    this.state.loading ?
                        <ActivityIndicator size="large" color={COLORS.primary} style={{ marginTop: 30 }} />
                        :
                        <ScrollView>
                            <View style={{ height: 100, backgroundColor: COLORS.primary }}></View>
                            <View style={{
                                backgroundColor: COLORS.white, margin: 15, marginTop: -80, marginBottom: 0, shadowColor: '#000',
                                shadowOffset: { width: 0, height: 1 },
                                shadowOpacity: 0.8,
                                shadowRadius: 2,
                                elevation: 5,
                                padding: 30,
                                zIndex: 999,
                                borderRadius: 8,
                            }}>
                                <View style={{ alignItems: 'center' }}>
                                    <TextInput type="text" maxLength={10} placeholder="RJ19AB0001" style={{
                                        borderWidth: 1, width: '100%', padding: 0, borderColor: '#ccc', paddingLeft: 10, paddingRight: 10, borderRadius: 8, shadowOpacity: 0, textAlign: 'center', fontWeight: 'bold'
                                    }}
                                        onChangeText={vehicleId => this.VehicleNumber(vehicleId)}
                                        value={this.state.vehicleId}
                                    />
                                    {
                                        this.state.user ?
                                            <TouchableOpacity onPress={() => this.SearchApi()} style={{ alignItems: 'center', marginTop: 20, marginBottom: 10, padding: 5, backgroundColor: COLORS.primary, width: 150, borderRadius: 8 }}><Text style={{ color: COLORS.white, }}>Search</Text></TouchableOpacity>
                                            :
                                            <TouchableOpacity onPress={() => this.navigateTo('Login1')} style={{ alignItems: 'center', marginTop: 20, marginBottom: 10, padding: 5, backgroundColor: COLORS.primary, width: 150, borderRadius: 8 }}><Text style={{ color: COLORS.white, }}>Search</Text></TouchableOpacity>
                                    }
                                </View>
                            </View>
                            <View style={{
                                backgroundColor: COLORS.white, margin: 15, shadowColor: '#000',
                                shadowOffset: { width: 0, height: 1 },
                                shadowOpacity: 0.8,
                                shadowRadius: 2,
                                elevation: 5,
                                padding: 15,
                                zIndex: 999,
                                borderRadius: 8,
                            }}>
                                {/* <View style={{ borderBottomColor: '#7b7d87', borderBottomWidth: .5 }}></View> */}
                                <Text style={{ color: COLORS.primary, fontWeight: 'bold', fontSize: 16 }}>{this.state.language == 'en' ? 'My Vehicles' : 'मेरे वाहन'}</Text>
                                <View style={style.Row}>
                                    {
                                        this.state.home && this.state.home.categories && this.state.home.categories.length && this.state.home.categories.map((cat, i) => {
                                            return (
                                                <View style={style.Column}>
                                                    <View style={{ backgroundColor: '#fff', borderColor: '#ccc', borderWidth: 1, borderRadius: 50, height: 40, width: 40, justifyContent: 'center', alignItems: 'center' }}>
                                                        <Text style={{ fontWeight: 'bold', fontSize: 12 }}>{cat.vehicle.length}</Text>
                                                    </View>
                                                    <Text style={{ textAlign: 'center', fontSize: 12, marginTop: 5 }}>{this.state.language == 'en' ? cat.name : cat.h_name}</Text>
                                                </View>
                                            )
                                        })
                                    }
                                </View>
                            </View>
                            {
                                this.state.home && this.state.home.sliders && this.state.home.sliders.length && this.state.home.sliders ?
                                    <View style={{ height: 150 }}>
                                        <Carousel
                                            ref={this.carouselRef}
                                            layout='default'
                                            data={this.state.home && this.state.home.sliders && this.state.home.sliders.length && this.state.home.sliders}
                                            sliderWidth={SCREEN.width}
                                            itemWidth={SCREEN.width - 30}
                                            renderItem={({ item, index }) => (
                                                <>
                                                    {/* <Text>{item.title}</Text> */}
                                                    <TouchableOpacity onPress={() => item.url != '' ? Linking.openURL(item.url) : ''}>
                                                        <Image
                                                            key={index}
                                                            style={{ width: '100%', height: '100%', borderRadius: 5 }}
                                                            resizeMode='cover'
                                                            source={{ uri: item.image_url }}
                                                        />
                                                    </TouchableOpacity>
                                                </>
                                            )}
                                        />
                                    </View>
                                    : ''
                            }
                            <View style={{
                                margin: 15, backgroundColor: COLORS.white, shadowColor: '#000',
                                shadowOffset: { width: 0, height: 1 },
                                shadowOpacity: 0.8,
                                shadowRadius: 2,
                                elevation: 5,
                                padding: 15,
                                borderRadius: 8
                            }}>
                                <Text style={{ color: COLORS.primary, fontWeight: 'bold', fontSize: 16 }}>{this.state.language == 'en' ? 'Expiring Documents' : 'समाप्त होने वाले दस्तावेज़'}</Text>
                                <View style={style.Row}>
                                    {
                                        this.state.home && this.state.home.documents && this.state.home.documents.length && this.state.home.documents.map((doc, j) => {
                                            return (
                                                <View style={style.Column1}>
                                                    <TouchableOpacity onPress={() => doc.total == 0 ? '' : this.redirectExpiryVehicle(doc.name)} style={{ backgroundColor: '#fff', borderColor: doc.total == 0 ? 'green' : doc.expired == 0 ? doc.expiring == 0 ? 'green' : 'orange' : 'red', borderWidth: 1, borderRadius: 50, height: 40, width: 40, justifyContent: 'center', alignItems: 'center' }}>
                                                        <Text style={{ fontWeight: 'bold', fontSize: 12 }}>{doc.vehicle_document.length}</Text>
                                                    </TouchableOpacity>
                                                    <Text style={{ textAlign: 'center', fontSize: 12, marginTop: 5 }}>{this.state.language == 'en' ? doc.name : doc.h_name}</Text>
                                                </View>
                                            )
                                        })
                                    }
                                </View>
                            </View>
                        </ScrollView>
                }
            </>
        );
    }
}

const style = StyleSheet.create({
    Row: {
        flexDirection: 'row',
        justifyContent: 'center',
        flexWrap: 'wrap'
    },
    Column: {
        width: (SCREEN.width - 50) / 5 - 10 / 5,
        alignItems: 'center',
        marginTop: 10,
    },
    Column1: {
        width: (SCREEN.width - 50) / 3 - 10 / 3,
        alignItems: 'center',
        marginTop: 10,
    }
});