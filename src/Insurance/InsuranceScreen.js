import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { ActivityIndicator, Image, Linking, Pressable, ScrollView, Text, TouchableOpacity, View } from "react-native";
import { Icon } from "react-native-elements";
import { commanApi } from "../../Api/api";
import { COLORS } from "../../configs/constants.config";

export default class InsuranceScreen extends Component {
    state = {
        loading: false,
        user: null,
        language: 'en',
        renew: []
    }

    navigateTo = (path, params = null) => {
        let { navigation } = this.props;

        navigation.navigate(path, params);
    }

    RenewApi = async () => {
        let { language } = this.state;
        let url = `renew?language=${language}`
        this.setState({ loading: true });
        let response = await commanApi(url);
        if (response.status) {
            if (response.data.status) {
                this.setState({ renew: response.data.data });
                setTimeout(() => {
                    this.setState({ loading: false });
                }, 30);
            } else {
                this.setState({ loading: false });
                Alert.alert(
                    "Error",
                    response.data.message,
                    [
                        { text: "OK", onPress: () => console.log("OK Pressed") }
                    ]
                );
            }
        }
    }
    async componentDidMount() {
        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            console.log('user details', user);
            this.setState({ user: user });
        }
        let lang = await AsyncStorage.getItem('@language');
        if (lang) {
            this.setState({ language: lang });
        }
        this.RenewApi();
    }

    render() {
        return (
            <>
                {
                    this.state.loading ?
                        <ActivityIndicator size="large" color={COLORS.primary} style={{ marginTop: 30 }} />
                        :
                        <ScrollView>
                            <View style={{ marginLeft: 15, marginRight: 15, marginBottom: 15 }}>
                                <View>
                                    {
                                        this.state.renew.map((item, index) => {
                                            return (
                                                <TouchableOpacity onPress={() => Linking.openURL(item.url)} style={{ marginTop: 15, backgroundColor: COLORS.white, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.8, shadowRadius: 2, elevation: 5, borderRadius: 8 }}>
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <View style={{ flex: 0.2, justifyContent: 'center', paddingTop: 10, paddingBottom: 10 }}>
                                                            {/* <Icon
                                                        name='car'
                                                        type='antdesign'
                                                        color='#000'
                                                        size={32}
                                                    /> */}
                                                            <Image
                                                                source={{ uri: item.image_url }}
                                                                resizeMode="contain"
                                                                style={{
                                                                    width: '100%',
                                                                    height: 30
                                                                }} />
                                                        </View>
                                                        <View style={{ flex: 0.8, paddingTop: 10, paddingBottom: 10 }}>
                                                            <Text>{item.name}</Text>
                                                            {/* <Text style={{ fontSize: 11 }}>Added 1 day ago</Text> */}
                                                        </View>
                                                    </View>
                                                </TouchableOpacity>
                                            )
                                        })
                                    }
                                    {/* <TouchableOpacity style={{ marginTop: 15, backgroundColor: COLORS.white, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.8, shadowRadius: 2, elevation: 5, borderRadius: 8 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <View style={{ flex: 0.2, justifyContent: 'center', paddingTop: 10, paddingBottom: 10 }}>
                                        <Icon
                                            name='motorcycle'
                                            type='materialicon'
                                            color='#000'
                                            size={32}
                                        />
                                    </View>
                                    <View style={{ flex: 0.8, paddingTop: 10, paddingBottom: 10 }}>
                                        <Text>Two Wheeler</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={{ marginTop: 15, backgroundColor: COLORS.white, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.8, shadowRadius: 2, elevation: 5, borderRadius: 8 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <View style={{ flex: 0.2, justifyContent: 'center', paddingTop: 10, paddingBottom: 10 }}>
                                        <Icon
                                            name='truck'
                                            type='feather'
                                            color='#000'
                                            size={32}
                                        />
                                    </View>
                                    <View style={{ flex: 0.8, paddingTop: 10, paddingBottom: 10 }}>
                                        <Text>Commercial</Text>
                                    </View>
                                </View>
                            </TouchableOpacity> */}
                                </View>
                            </View>
                        </ScrollView>
                }
            </>
        );
    }
}